package LK;

import java.util.Scanner;
import java.util.Stack;

class LettersPositions {
	
	private static final int numberOfLetters = 26;

	private Stack[] alphabet = new Stack[numberOfLetters];
	
	private void pushLetterPosition(char letter, int positionOfTheLetter)
	{
		int pointer = 'Z' - letter;
		alphabet[pointer].push(positionOfTheLetter);
	}
	
	public void loadLettersPositionsFromWord(String word)
	{
		for(int i = word.length(); i > 0; --i)
		{
			char letter = word.charAt(i-1);
			pushLetterPosition(letter, i);
		}
	}
	
	public int popLetterPosition(char letter)
	{
		int pointer = 'Z' - letter;
		return (int) alphabet[pointer].pop();
	}
	
	public LettersPositions()
	{
		for(int i = 0; i < numberOfLetters; ++i)
		{
			alphabet[i] = new Stack();
		}
	}
}

class IntervalTree {
	
	private int _numberOfAllElements;
	
	private static final int MAX = 1000000;
	
	private int wordPositions[];
	
	public void increment(int position)
	{
		int nodePointer = _numberOfAllElements + position; 
		++wordPositions[nodePointer];
		while(nodePointer != 1)
		{
			nodePointer /= 2;
			++wordPositions[nodePointer];
		}
	}
	
	public long sumOfElements(int startElementPosition, int endElementPosition)
	{
		if(startElementPosition > endElementPosition) return 0;
		int startPointer = _numberOfAllElements + startElementPosition;
		int endPointer = _numberOfAllElements + endElementPosition;
		long result = wordPositions[startPointer];
		if (startPointer != endPointer) result += wordPositions[endPointer];
		while(startPointer / 2 != endPointer / 2)
		{
			if (startPointer % 2 == 0) result += wordPositions[startPointer + 1];
			if (endPointer % 2 == 1) result += wordPositions[endPointer - 1];
			startPointer /= 2;
			endPointer /= 2;
		}
		return result;
	}
	
	public IntervalTree(int numberOfAllElements)
	{
		_numberOfAllElements = numberOfAllElements;
		wordPositions = new int[2*MAX+1];
	}
}

class TransitionCounter {
	
	public long countTransitions(int numberOfLetters, String wordA, String wordB)
	{
		LettersPositions lettersPositions = new LettersPositions();
		IntervalTree inTree = new IntervalTree(numberOfLetters);
		
		lettersPositions.loadLettersPositionsFromWord(wordA);
		
		long result = 0;
		for(int i = 0; i < numberOfLetters; ++i)
		{
			char letter = wordB.charAt(i);
			int position = lettersPositions.popLetterPosition(letter);
			inTree.increment(position);
			position -= inTree.sumOfElements(1, position - 1);
			result += position - 1;
		}
		return result;
	}
	
}

public class Main {

	public static void main(String[] args) {
		
		int numberOfLetters;
		
		String wordA;
		String wordB;
		
		Scanner value = new Scanner(System.in);
		
		numberOfLetters = Integer.parseInt(value.nextLine());
		wordA = value.nextLine();
		wordB = value.nextLine();
		
		long result = new TransitionCounter().countTransitions(numberOfLetters, wordA, wordB);
		
		System.out.println(result);
	}

}

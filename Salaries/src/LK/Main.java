package LK;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Stack;

class ResultsPrinter
{
	public static void printResults(int numberOfNodes, int[] resultTable, boolean[] isUnknown) 
	{
		for(int i = 1; i <= numberOfNodes; ++i)
		{
			if(isUnknown[resultTable[i]])
			{
				System.out.println(0);
			}
			else
			{
				System.out.println(resultTable[i]);
			}
		}
	}
}

class SalaryFiller
{
	private int skipUsedValues(ResultsHolder results, int pointer)
	{
		while(results.isUsed[pointer])
		{
			++pointer;
		}
		return pointer;
	}
	
	private void clearStackAndMarkLowerValuesAsUnknown(ResultsHolder results, Stack<Integer> subTreeValues,
			int rootId, int lowestValue) 
	{
		while(subTreeValues.size() > 0)
		{
			results.isUnknown[subTreeValues.pop()] = true;
		}
		for(int i = lowestValue; i < results.nodeValues[rootId]; )
		{
			results.isUnknown[i] = true;
			++i;
			i = skipUsedValues(results,i);
		}
	}
	
	private int returnOneChildUnknown(ResultsHolder results, int nodeId)
	{
		int knownChildren = 0;
		int knownChildNr = 0;
		for(int i = 0; i < results.childrenId[nodeId].size(); ++i)
		{
			if(results.isUsed[results.nodeValues[(int)results.childrenId[nodeId].get(i)]])
			{
				++knownChildren;
			}
			else
				knownChildNr = i;
		}
		if(results.childrenId[nodeId].size() - knownChildren == 1)
			return knownChildNr;
		else
			return -1;
	}
	
	private void fillAllAvailableNodesWithSalaries(ResultsHolder results, Stack<Integer> subTreeValues, int rootId)
	{
		int knownChildNr = 0;
		while((knownChildNr = returnOneChildUnknown(results, rootId)) >= 0)
		{
			rootId = (int)results.childrenId[rootId].get(knownChildNr);
			results.nodeValues[rootId] = subTreeValues.pop();
		}
	}
	
	public void fillAllSubTrees(ResultsHolder results) 
	{
		int lowestValue = 1; // the lowest value available to set in a node
		
		while(results.subTreeRoots.size() > 0 )
		{
			int rootId = results.subTreeRoots.poll().getNodeId();
			if(results.unknownChildren[rootId] == 0) continue;
			Stack<Integer> subTreeValues = new Stack<Integer>();
			
			lowestValue = skipUsedValues(results,lowestValue);
			
			for(int i = 1; i <= results.unknownChildren[rootId] && lowestValue < results.numberOfNodes ; ++i)
			{
				subTreeValues.push(lowestValue);
				++lowestValue;
				lowestValue = skipUsedValues(results,lowestValue);
			}
			
			if(results.nodeValues[rootId] > lowestValue)
			{
				clearStackAndMarkLowerValuesAsUnknown(results, subTreeValues, rootId, lowestValue);
			}
			else
			{
				fillAllAvailableNodesWithSalaries(results, subTreeValues, rootId);
				subTreeValues.clear();
			}
		}
	}
}

class Node
{
	private int value;
	private int nodeId;
	
	public int getValue()
	{
		return value;
	}
	
	public int getNodeId()
	{
		return nodeId;
	}
	
	public Node(int nodeId, int value)
	{
		this.nodeId = nodeId;
		this.value = value;
	}
}

class NodeComparator implements Comparator<Node>
{
	@Override
	public int compare(Node n1, Node n2) {
		
		if (n1.getValue() > n2.getValue()) {
	           return 1;
	       } else if (n1.getValue() < n2.getValue()){
	           return -1;
	       } else {
	           return 0;
	       }
	}
}

class DataLoader
{
	
	private boolean isRoot(ResultsHolder results, int nodeId)
	{
		return results.parentId[nodeId] == nodeId;
	}
	
	private boolean isNodeValueUnknown(ResultsHolder results, int nodeId)
	{
		return results.nodeValues[nodeId] == 0;
	}
	
	private void addChild(ResultsHolder results, int nodeId)
	{
		results.childrenId[results.parentId[nodeId]].add(nodeId);
	}
	
	private int setUnknownChildren(ResultsHolder results, int rootId)
	{
		int totalUnknownChildren = 0;
		for(int i = 0; i < results.childrenId[rootId].size(); ++i)
		{
			totalUnknownChildren += setUnknownChildren(results, (int)results.childrenId[rootId].get(i));
		}
		results.unknownChildren[rootId] = totalUnknownChildren;
		if(results.nodeValues[rootId] == 0)
			return totalUnknownChildren + 1;
		else
			return 0;
	}
	
	public void LoadDataFrom(ResultsHolder results, Scanner scanner) 
	{
		int rootId = 0;
		results.numberOfNodes = Integer.parseInt(scanner.nextLine());
		results.instantiateLists();
		for(int i = 1; i <= results.numberOfNodes; ++i)
		{
			String twoIntegers = scanner.nextLine();
			String[] parentAndValue = twoIntegers.split(" ");
			results.nodeValues[i] = Integer.parseInt(parentAndValue[1]);
			if(!isNodeValueUnknown(results,i))
			{
				results.isUsed[results.nodeValues[i]] = true;
				results.subTreeRoots.add(new Node(i, results.nodeValues[i]));
			}
			results.parentId[i] = Integer.parseInt(parentAndValue[0]);
			if(isRoot(results,i))
			{
				if(isNodeValueUnknown(results,i)) results.nodeValues[i] = results.numberOfNodes;
				rootId = i;
				continue;
			}
			addChild(results,i);
		}
		setUnknownChildren(results, rootId);
	}
}

class ResultsHolder
{
	private final static int MAX = 1000000;
	
	public static int getMAXQuantity()
	{
		return MAX;
	}
	
	public int numberOfNodes;
	public boolean[] isUnknown = new boolean[MAX+1];
	public boolean[] isUsed = new boolean[MAX+2]; // +2 set to protect from iterating outside the array
	public int[] unknownChildren = new int[MAX+1];
	public LinkedList[] childrenId = new LinkedList[MAX+1];
	public int[] parentId = new int[MAX+1];
	public int[] nodeValues = new int[MAX+1];
	
	public PriorityQueue<Node> subTreeRoots = new PriorityQueue<Node>(getMAXQuantity(), new NodeComparator());
	
	public void instantiateLists()
	{
		for(int i = 1; i <= numberOfNodes; ++i)
		{
			childrenId[i] = new LinkedList();
		}
	}
}

public class Main {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		ResultsHolder results = new ResultsHolder();
		DataLoader dl = new DataLoader();
		dl.LoadDataFrom(results, scanner);
		
		SalaryFiller sr = new SalaryFiller();
		sr.fillAllSubTrees(results);
		
		ResultsPrinter.printResults(results.numberOfNodes, results.nodeValues, results.isUnknown);
	}
}
